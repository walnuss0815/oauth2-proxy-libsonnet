local k = import 'k.libsonnet';
local u = import 'jsonnet-libs/ksonnet-util/util.libsonnet';

{
  _config:: {
    name: 'oauth2-proxy',
    cookieSecretEnvVar: error 'Must define a cookie secret',
    clientIdEnvVar: error 'Must define a client id',
    clientSecretEnvVar: error 'Must define a client secret',
    provider: 'oidc',
    redirectUrl: error 'Must define a redirect url',
    upstream: error 'Must define an upstream',
    issuerUrl: error 'Must define an issuer url',
    emailDomain: '*',
    oidcScopes: ['openid'],
    extraArgs: [],
  },

  _images+:: {
    oauth2_proxy: 'quay.io/oauth2-proxy/oauth2-proxy:v7.4.0',
  },

  new(upstream, name='oauth2-proxy')::
    self +
    {
      _config+:: {
        name: name,
        upstream: upstream,
      },
    },

  local container = k.core.v1.container,
  local containerPort = k.core.v1.containerPort,
  local envVar = k.core.v1.envVar,

  oauth2_proxy_container::
    container.new($._config.name, $._images.oauth2_proxy) +
    container.withPorts(containerPort.newNamed(4180, 'http')) +
    container.withArgs([
      '--http-address=0.0.0.0:4180',
      '--provider=%s' % $._config.provider,
      '--redirect-url=%s' % $._config.redirectUrl,
      '--upstream=%s' % $._config.upstream,
      '--oidc-issuer-url=%s' % $._config.issuerUrl,
      '--email-domain=%s' % $._config.emailDomain,
      '--scope=%s' % std.join(' ', $._config.oidcScopes),
    ] + $._config.extraArgs) +
    container.withEnv(
      [
        $._config.cookieSecretEnvVar + envVar.withName('OAUTH2_PROXY_COOKIE_SECRET'),
        $._config.clientIdEnvVar + envVar.withName('OAUTH2_PROXY_CLIENT_ID'),
        $._config.clientSecretEnvVar + envVar.withName('OAUTH2_PROXY_CLIENT_SECRET'),
      ]
    ) +
    container.readinessProbe.httpGet.withPath('/ping') +
    container.readinessProbe.httpGet.withPort('http') +
    container.readinessProbe.httpGet.withScheme('HTTP') +
    container.livenessProbe.httpGet.withPath('/ping') +
    container.livenessProbe.httpGet.withPort('http') +
    container.livenessProbe.httpGet.withScheme('HTTP'),

  local deployment = k.apps.v1.deployment,

  oauth2_proxy_deployment:: deployment.new('oauth2-proxy', 1, [$.oauth2_proxy_container]),

  oauth2_proxy_service:: u.serviceFor($.oauth2_proxy_deployment),


  flatten()::
    [$.oauth2_proxy_deployment, $.oauth2_proxy_service],

  withCookieSecretEnvVar(cookieSecretEnvVar):: {
    _config+:: {
      cookieSecretEnvVar: cookieSecretEnvVar,
    },
  },

  withClientIdEnvVar(clientIdEnvVar):: {
    _config+:: {
      clientIdEnvVar: clientIdEnvVar,
    },
  },

  withClientSecretEnvVar(clientSecretEnvVar):: {
    _config+:: {
      clientSecretEnvVar: clientSecretEnvVar,
    },
  },

  withProvider(provider):: {
    _config+:: {
      provider: provider,
    },
  },

  withRedirectUrl(redirectUrl):: {
    _config+:: {
      redirectUrl: redirectUrl,
    },
  },

  withUpstream(upstream):: {
    _config+:: {
      upstream: upstream,
    },
  },

  withIssuerUrl(issuerUrl):: {
    _config+:: {
      issuerUrl: issuerUrl,
    },
  },

  witheMailDomain(emailDomain):: {
    _config+:: {
      emailDomain: emailDomain,
    },
  },

  withExtraArgs(extraArgs):: {
    _config+:: {
      extraArgs: extraArgs,
    },
  },

  withExtraArgsMixin(extraArgs):: {
    _config+:: {
      extraArgs+: extraArgs,
    },
  },

  withOidcScopes(oidcScopes):: {
    _config+:: {
      oidcScopes: oidcScopes,
    },
  },

  withOidcScopesMixin(oidcScopes):: {
    _config+:: {
      oidcScopes+: oidcScopes,
    },
  },
}
