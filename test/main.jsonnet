local p = import '../src/main.libsonnet';
local k = import 'k.libsonnet';

local envVar = k.core.v1.envVar;

{
  standard: {
    actual:: (
      p.new('http://hello-world-service') +
      p.withCookieSecretEnvVar(envVar.new('key', 'value')) +
      p.withClientIdEnvVar(envVar.new('key', 'value')) +
      p.withClientSecretEnvVar(envVar.new('key', 'value')) +
      p.withRedirectUrl('https://helloworld.visprod.de/oauth2/callback') +
      p.withIssuerUrl('https://keycloak.visprod.de/realms/cluster')
    ).flatten(),
    expected:: std.parseYaml(importstr 'expected/standard.yaml'),
    success: std.assertEqual(self.actual, self.expected),
  },
}
